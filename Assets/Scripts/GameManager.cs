﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
	public Text LevelNow;
	public Text LevelNext;

	public Text Blocks;

	public int LNow = 1;
	public int LNext = 2;

	public int blocksMax;
	public int blocks;

	public Image Progress;

	public GameObject Next;
	public GameObject Restart;

	private GameObject Weapon;
	private Weapon wp;
	
	public float timer = 5f;
	public bool tim = false;

	private void Start()
	{
		Weapon = GameObject.Find("Canon");
		wp = Weapon.GetComponent<Weapon>();
	}

	private void Update()
	{
		if (blocks == blocksMax)
		{
			Next.SetActive(true);
			tim = true;
		}

		if (wp.Ammo <= 0)
		{
			timer -= Time.deltaTime;
			if (timer <= 0 && blocks != blocksMax)
			{
				Restart.SetActive(true);
				timer = 10f;
				tim = true;
			}
		}
	}
}
