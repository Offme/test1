﻿using System;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManagerGame : MonoBehaviour
{
	private GameManager GM;
	private GameObject GameM;
	
	private GameObject Weapon;
	private Weapon wp;

	public GameObject Next;
	public GameObject Lost;

	public float ev;

	private void Awake()
	{
		if (!FB.IsInitialized)
		{
			FB.Init(InitCallback, OnHideUnity);
		}
		else
		{
			FB.ActivateApp();
		}
	}
	
	private void Start()
	{
		GameM = GameObject.Find("GameManager");
		GM = GameM.GetComponent<GameManager>();
		
		Weapon = GameObject.Find("Canon");
		wp = Weapon.GetComponent<Weapon>();
		
		GM.LNow = SceneManager.GetActiveScene().buildIndex;
		Debug.Log(GM.LNow);
		GM.LNext = GM.LNow + 1;
		wp.MaxAmmo = 3 * GM.LNow;
		wp.Ammo = wp.MaxAmmo;
		GM.LevelNow.text = "" + GM.LNow;
		GM.LevelNext.text = "" + GM.LNext;
		wp.AmmoText.text = wp.Ammo + " / " + wp.MaxAmmo;
		
	}

	private void OnHideUnity (bool isGameShown)
	{
		if (!isGameShown) {
			// Pause the game - we will need to hide
			Time.timeScale = 0;
		} else {
			// Resume the game - we're getting focus again
			Time.timeScale = 1;
		}
	}
	
	private void InitCallback()
	{
		if (FB.IsInitialized)
		{
			FB.ActivateApp();
		}
		else
		{
			Debug.Log("Failed to start FB SDK");
		}
	}

	private void LevelComplete()
	{
		var tutParams = new Dictionary<string, object>();
		tutParams[AppEventParameterName.ContentID] = "Level";
		tutParams[AppEventParameterName.Level] = GM.LNow;
		
		FB.LogAppEvent(AppEventName.AchievedLevel, parameters: tutParams);
	}
	

	public void NextBut()
	{
		LevelComplete();
		
		Application.LoadLevel(GM.LNext);
		/*GM.blocks = 0;
		GM.tim = false;

		GM.Spawn();
		Next.SetActive(false);*/
	}

	public void RestartBut()
	{
		Application.LoadLevel(GM.LNow);
		
		/*GM.tim = false;
		GM.blocks = 0;
		GM.blocksMax = 0;
		wp.MaxAmmo = 3 * GM.LNow;
		wp.Ammo = wp.MaxAmmo;
		
		GM.LevelNow.text = "" + GM.LNow;
		GM.LevelNext.text = "" + GM.LNext;
		wp.AmmoText.text = wp.Ammo + " / " + wp.MaxAmmo;
		GM.SpawnOld();
		Lost.SetActive(false);*/
	}

	public void Menu()
	{
		Application.LoadLevel(0);
	}
}
