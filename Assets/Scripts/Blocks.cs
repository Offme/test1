﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blocks : MonoBehaviour
{
    private MeshRenderer meshRenderer;

    public bool col = false;

    public float speed = .5f;
    
    private GameManager GM;
    private GameObject GameM;

    private float fill;

    private void Start(){
        meshRenderer = this.GetComponent<MeshRenderer>();
        
        GameM = GameObject.Find("GameManager");
        GM = GameM.GetComponent<GameManager>();

        GM.blocksMax++;
        GM.Blocks.text = GM.blocks + " / " + GM.blocksMax;
    }

    private float t = 0.0f;

    private void Update()
    {
        if (col == true)
        {
            Material[] mats = meshRenderer.materials;

            mats[0].SetFloat("_Cutoff", Mathf.Sin(t * speed));
            t += Time.deltaTime;

            // Unity does not allow meshRenderer.materials[0]...
            meshRenderer.materials = mats;
            if (t >= 2f)
            {
                fill = GM.blocksMax * 0.01f;
                GM.blocks++;
                GM.Blocks.text = GM.blocks + " / " + GM.blocksMax;
                GM.Progress.fillAmount += fill;
                Debug.Log(fill);
                Destroy(gameObject);
            }
        }

    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            col = true;
        }
    }
}
