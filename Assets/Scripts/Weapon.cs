﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Weapon : MonoBehaviour
{
    private GameObject target;
    private Camera mainCamera;
    private GameManager GM;
    private GameObject GameM;
    
    public Rigidbody Bullet;

    public float force = 10;
    public int MaxAmmo = 3;
    public int Ammo = 3;

    public Text AmmoText;
    
    void Start()
    {
        GameM = GameObject.Find("GameManager");
        GM = GameM.GetComponent<GameManager>();
        mainCamera = Camera.main;
        target = GameObject.Find("Target");
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) == true && Ammo > 0)
        {
            RaycastHit hit;
            if (Physics.Raycast(mainCamera.ScreenPointToRay(Input.mousePosition), out hit))
            {
                Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance,
                    Color.yellow);
            }
            transform.LookAt(hit.point);
            
            Rigidbody Bul = Instantiate(Bullet, target.transform.position, transform.rotation, transform.parent);
            
            Bul.AddForce(Bul.transform.forward * force, ForceMode.Impulse);
            
            Ammo--;
            AmmoText.text = Ammo + " / " + MaxAmmo;
        }
    }
}
